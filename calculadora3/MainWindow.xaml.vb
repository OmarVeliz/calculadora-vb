﻿Imports System.Text.RegularExpressions
Class MainWindow
    Dim _cadena As String
    Dim operacion As String
    Dim x As Double = "1"
    Dim Variable_ As String = ""

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)

        operacion = _cadena + CType(sender, Button).Content

        Select Case CType(sender, Button).Content

            Case "="
                _cadena = Eval(_cadena).ToString("N")
                mostrar.Text = _cadena
                _cadena = vbNullString
            Case "CE"
                _cadena = ("")
                mostrar.Text = _cadena
                _cadena = null
            Case "←"

                Dim newString As String = Mid(_cadena, 1, _cadena.Length - 1)

                _cadena = newString
                mostrar.Text = _cadena

            Case "1/x"
                _cadena = (x / _cadena)
                mostrar.Text = _cadena

            Case "+/-"

            Case Else
                _cadena = _cadena + CType(sender, Button).Content
                mostrar.Text = _cadena

        End Select
    End Sub

    Public Function Eval(ByVal operacion As String) As Double
        Dim ans As Double = 0D
        Dim tard As Double = 0D

        operacion = operacion.Replace(".", ",")

        Dim RegexObj As New Regex _
            ("(?<Termino>[\+\-]?(?:(?:\d+(?:\,\d*)?|\([\d\,\+\-\/\*]*\))(?:[\*\/](?:\d+(?:\,\d*)?|\([\d\,\+\-\/\*]+\)))*))")

        If RegexObj.IsMatch(operacion) Then
            Dim MatchResults As MatchCollection = RegexObj.Matches(operacion)
            Dim MatchResult As Match = MatchResults(0)
            Dim termino As String

            For i As Int32 = 0 To MatchResults.Count - 1

                termino = MatchResult.Groups("Termino").Value
                If IsNumeric(termino) Then

                    tard = Double.Parse(termino)
                Else

                    Dim signo As Integer = 1

                    If termino.Substring(0, 1) = "-" Then
                        signo = -1
                        termino = termino.Substring(1)
                    ElseIf termino.Substring(0, 1) = "+" Then
                        signo = 1
                        termino = termino.Substring(1)
                    End If

                    tard = operacionT(termino)

                    tard *= signo
                End If


                ans += tard
                MatchResult = MatchResult.NextMatch()
            Next

            Return ans
        Else
            Throw New FormatException("Error")
        End If
    End Function

    Private Function operacionT(ByVal Termino As String) As Double
        Dim ans As Double = 0D
        Dim tard As Double = 0D

        Dim RegexObj As New Regex( _
            "(?<Termino>[\+\-]? \( [\d\,\+\-\*\/\(\)]+ \)|[\*\/] (?: \( [\d\+\-\*\/\(\)]* \) | \d*\,?\d* )|[\+\-]? \d* (?:\,\d*)?)", _
            RegexOptions.IgnorePatternWhitespace)


        If RegexObj.IsMatch(Termino) Then

            Dim MatchResults As MatchCollection = RegexObj.Matches(Termino)
            Dim MatchResult As Match = MatchResults(0)
            Dim subTermino As String
            For I As Int32 = 0 To MatchResults.Count - 2
                subTermino = MatchResult.Groups("Termino").Value
                '
                If IsNumeric(subTermino) Then
                    ans = Double.Parse(subTermino)
                Else

                    If subTermino.Contains("^") Then
                        Dim regexPotencia As String = "^(?<Base>[+\-]?\d\,?\d*|[+\-]?\([+\-]?\d[\d,+*/\-\^]*\))\^(?<Potencia>[+\-]?\d\,?\d*|[+\-]?\([+\-]?\d[\d,+*/\-\^]*\))$"

                        Dim base As Double = Eval(Regex.Match(subTermino, regexPotencia).Groups("Base").Value)
                        Dim potencia As Double = Eval(Regex.Match(subTermino, regexPotencia).Groups("Potencia").Value)

                        ans = Math.Pow(base, potencia)
                    Else

                        Select Case subTermino.Substring(0, 1)
                            Case "*"
                                tard = Eval(subTermino.Substring(1))
                                ans *= tard
                            Case "/"
                                tard = Eval(subTermino.Substring(1))
                                ans /= tard
                            Case Else
                                ans = Eval(Regex.Match(subTermino, "\((?<Operacion>.*)\)").Groups("Operacion").Value)
                        End Select
                    End If
                End If
                MatchResult = MatchResult.NextMatch
            Next
            Return ans
        Else
            Throw New FormatException("Parte de la operacion no pudo ser reconocida")
        End If
    End Function
End Class